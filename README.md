# Waves Node in Docker

## Tag

- 1.1.10 ([wavesplatform/wavesnode:1.1.10](https://hub.docker.com/r/wavesplatform/wavesnode/tags))

## About Waves
Waves is a decentralized platform that allows any user to issue, transfer, swap and trade custom blockchain tokens on an integrated peer-to-peer exchange. You can find more information about Waves at [wavesplatform.com](https://wavesplatform.com) and in the official [documentation]((https://docs.wavesplatform.com)).


## About the image
This Docker image contains scripts and configs to run Waves Node from version 0.13.0 for TESTNET, MAINNET or CUSTOM networks.
The image is focused on fast and convenient deployment of Waves Node.

Container downloads `.jar` file and configuration files from the [releases section](https://github.com/wavesplatform/Waves/releases) and runs it.  
 

## Running the image

It is highly recommended to read more about [Waves Node configuration](https://docs.wavesplatform.com/en/waves-full-node/how-to-configure-a-node.html) before running the container.

You must run a container with predefined environment variables.

Create `.env` file with following variables:

| Env variable                    | Description                                                                |
| ------------------------------- | -------------------------------------------------------------------------- |
| `WALLET_SEED`                   | Plain text seed for node wallet. Container converts it to base58.          |
| `WALLET_PASSWORD`               | Password for wallet file.                                                 |
| `REST_API_KEY_HASH`             | Key hash for a Rest api.                                                  |
| `PATH_TO_DATA_FOLDER`           | Full path to the blockchain data folder.                                  |
| `PATH_TO_CONFIG_FOLDER`         | Full path to the wallet configuration folder.                             |

**Note: WALLET_SEED and WALLET_PASSWORD variables are optional.**  
